<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    protected $fillable = [
        'id','code', 'short_text', 'long_text', 'start_date', 'end_date', 'file', 'active', 'created_at', 'created_by'
    ];
}
