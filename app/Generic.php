<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Generic extends Model
{
    //
    protected $fillable = [
        'id','code', 'parent_code', 'name',
    ];
}
