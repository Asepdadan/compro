<?php

namespace App\Http\Controllers;

use Auth;
use App\Cms;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use DB;
use Config;
use Intervention\Image\ImageManagerStatic as Image;



class CmsController extends Controller
{
    public function index()
    {
        return view('cms.index');
    }

    public function add($id="")
    {
        $data['id'] = $id;
        return view('cms.add',$data);
    }
    
    public function get_datatables()
    {
        $cms = Cms::select(['cms.id','generics.name as code', 'cms.short_text', 'cms.long_text', 'cms.start_date', 'cms.end_date', 'cms.file', 'cms.active', 'cms.created_at','cms.created_by'])
        ->join('generics','generics.code','=','cms.code');

        return Datatables::of($cms)
            ->addColumn('active', function ($cms) {
                if($cms->active == 1){
                    return "<span class='badge badge-success'>Aktif</badge>";
                }else{
                    return "<span class='badge badge-danger'>Tidak Aktif</badge>";
                }
            }) 
            ->addColumn('file', function ($cms) {
                
                if($cms->file!=null || $cms->file!=""){
                    
                    return '<a href="'.asset('assets/uploads/'.$cms->file).'" data-lightbox="image-'.$cms->id.'" data-title="image-'.$cms->short_text.'"><img height="100" width="100" src="'.asset('assets/uploads/'.$cms->file).'" class="img img-circle"></a>';
                }elseif($cms->file != 'undefined'){
                    return "";
                }else{
                    return "";
                }
            })
            ->addColumn('create_by', function ($cms) {
                return \App\User::find($cms->created_by)->name;
            })
            ->addColumn('action', function ($cms) {
                return '<a href="'.url('cms/add/'.$cms->id).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a> <a href="javascript:;" onclick="delete_row('.$cms->id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>';
            })
            ->rawColumns(['active', 'action', 'file'])
            ->removeColumn('updated_at')
            ->make(true);
    }

    public function get_edit($id=""){
        $data = [];

        if($id!=""){
            $cms = DB::table('cms as a')
            ->join('generics as b', function ($join) {
                $join->on('a.code', '=', 'b.code');
            })
            ->select('a.*', 'b.name as generic_name')
            ->where('a.id', '=', $id)
            ->first();
            $cms = json_decode(json_encode($cms),true);

            if($cms['start_date'] != NULL){
                $cms['start_date'] = date("d-m-Y",strtotime($cms['start_date']));
            }

            if($cms['end_date'] != NULL){
                $cms['end_date'] = date("d-m-Y",strtotime($cms['end_date']));
            }
        
            $data['edit'] = $cms;
        }
        
        return response()->json($data);
    }
          
    public function create(Request $request)
    {
        // $file = $_FILES['file'];
        //     $insert_id = date('d-m-YHis');
        //     $tempFile = $file['tmp_name'];
        //     $extension = $request->file('file')->extension();
        // $uploads_dir = Config::get('app.upload_dir');            
        // $img = Image::make('assets/uploads/06-09-2018000109.jpeg')->resize(300, 200);
        // return $img->response($extension);
        // die;

        $data = request()->all();
        unset($data['X-CSRF-TOKEN']);
        //echo Auth::user()->id;die;
        if(isset($data['start_date'])){
            $data['start_date'] = date('Y-m-d', strtotime($data['start_date']));
        }

        if(isset($data['end_date'])){
            $data['end_date'] = date('Y-m-d', strtotime($data['end_date']));
        }
        
        if(! empty($_FILES) && isset($_FILES['file']['name'])){
            $file = $_FILES['file'];
            $insert_id = date('d-m-YHis');
            $tempFile = $file['tmp_name'];
            $extension = $request->file('file')->extension();
            $uploads_dir = Config::get('app.upload_dir');            
            
            
            $data['file'] = $insert_id.".".$extension;
            $image       = $request->file('file');
            $img = Image::make($image->getRealPath())->resize(300, 200);
            $img->save(public_path("assets/uploads/".$insert_id.".".$extension));
            //move_uploaded_file($tempFile, "$uploads_dir/$insert_id.".$extension);
            //$manager->make($data['file'])->resize(300, 200);
        }else{
            unset($data['file']);
        }
                
        if($data['id'] == '0' || empty($data['id'])){
            $data['created_by'] = Auth::user()->id;
            
            $success  = Cms::create($data);
        }else{
            $data['created_by'] = Auth::user()->id;
            $success  = Cms::where('id', $data['id'])
            ->update($data);
        }

        $response = [
            "errorId" => $success ? 0 : 1,
            "message" => $success ? "Data berhasil disimpan" : "Data gagal disimpan"
        ];

        return response()->json($response);
    }

    public function delete(Request $request)
    {
        $id = Input::get('id');
        $success = Cms::where("id", $id)->delete();
        
        $response = [
            "errorId" => $success ? 0 : 1,
            "message" => $success ? "Data berhasil disimpan" : "Data gagal disimpan"
        ];

        return response()->json($response);
    }

    public function get_front($config){
        
        if($config == config('kode.galeri')){
            $response = \App\Cms::where('code', $config)->where('active', 1)->where('file','!=', null)->get();
        }else{
            $response = \App\Cms::where('code', $config)->where('active', 1)->first();
        }       
        
        
        return response()->json($response);
    }


}
