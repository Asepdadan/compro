<?php

namespace App\Http\Controllers;


use App\Generic;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use DB;

class GenericController extends Controller
{
    public function index()
    {
        return view('generic.index');
    }

    public function add($id="")
    {
        $data['id'] = $id;
        return view('generic.add',$data);
    }
    
    public function get_datatables()
    {
        $users = Generic::select(['id', 'code', 'parent_code', 'name', 'active','created_at', 'updated_at']);

        return Datatables::of($users)
            ->addColumn('active', function ($user) {
                if($user->active == 1){
                    return "<span class='badge badge-success'>Aktif</badge>";
                }else{
                    return "<span class='badge badge-danger'>Tidak Aktif</badge>";
                }
            })    
            ->addColumn('action', function ($user) {
                return '<a href="'.url('generics/add/'.$user->id).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a> <a href="javascript:;" onclick="delete_row('.$user->id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>';
            })
            ->addColumn('created_at', function ($user) {
                return date('d-m-Y H:i:s',strtotime($user->created_at));
            })
            ->rawColumns(['active', 'action'])
            ->removeColumn('updated_at')
            ->make(true);
    }

    public function get_edit($id=""){
        $data = [];

        if($id!=""){
            $users = DB::table('generics as a')
            ->join('generics as b', function ($join) {
                $join->on('a.parent_code', '=', 'b.code');
            })
            ->select('a.*', 'b.name as parent_name')
            ->where('a.id', '=', $id)
            ->first();

            $data['edit'] = $users;
        }
        
        return response()->json($data);
    }

    public function get_parent(Request $request){
        $data = Generic::select(['id', 'code', 'parent_code', 'name'])
            ->Where('name', 'like', '%' . Input::get('q') . '%')
            ->get();
        
        return response()->json($data);
    } 
    
    public function create(Request $request)
    {
        $data = request()->all();
        unset($data['X-CSRF-TOKEN']);
        //print_r($data);die;
        if($data['id'] == '0' || empty($data['id'])){
            $success  = Generic::create($data);
        }else{
            $success  = Generic::where('id', $data['id'])
                ->update($data);
        }

        $response = [
            "errorId" => $success ? 0 : 1,
            "message" => $success ? "Data success saved" : "Data failed save"
        ];

        return response()->json($response);
    }

    public function delete(Request $request)
    {
        $id = Input::get('id');
        $success = Generic::where("id", $id)->delete();
        
        $response = [
            "errorId" => $success ? 0 : 1,
            "message" => $success ? "Data success saved" : "Data failed save"
        ];

        return response()->json($response);
    }
}
