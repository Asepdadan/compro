<?php

namespace App\Http\Controllers;

use App\Kegiatan;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Auth;

class KegiatanController extends Controller
{
public function index()
    {
        return view('kegiatan.index');
    }

    public function add($id="")
    {
        $data['id'] = $id;
        return view('kegiatan.add',$data);
    }
    
    public function get_datatables()
    {
        $kegiatan = Kegiatan::select(['id','judul', 'waktu', 'tempat', 'keterangan']);

        return Datatables::of($kegiatan)
            ->addColumn('action', function ($kegiatan) {
                return '<a href="'.url('kegiatan/add/'.$kegiatan->id).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a> <a href="javascript:;" onclick="delete_row('.$kegiatan->id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>';
            })
            ->rawColumns(['action'])
            ->removeColumn('updated_at')
            ->make(true);
    }

    public function get_edit($id=""){
        $data = [];

        if($id!=""){
            $kegiatan = DB::table('kegiatan')->where('id', '=', $id)
            ->first();
            $kegiatan = json_decode(json_encode($kegiatan),true);

            /*if($kegiatan['start_date'] != NULL){
                $kegiatan['start_date'] = date("d-m-Y",strtotime($kegiatan['start_date']));
            }

            if($kegiatan['end_date'] != NULL){
                $kegiatan['end_date'] = date("d-m-Y",strtotime($kegiatan['end_date']));
            }*/
        
            $data['edit'] = $kegiatan;
        }
        
        return response()->json($data);
    }
          
    public function create(Request $request)
    {
     $data = request()->all();
        unset($data['X-CSRF-TOKEN']);
        //print_r($data);die;
        if($data['id'] == '0' || empty($data['id'])){
            $success  = Kegiatan::create($data);
        }else{
            $success  = Kegiatan::where('id', $data['id'])
                ->update($data);
        }

        $response = [
            "errorId" => $success ? 0 : 1,
            "message" => $success ? "Data success saved" : "Data failed save"
        ];

        return response()->json($response);
    }

    public function delete(Request $request)
    {
        $id = Input::get('id');
        $success = Kegiatan::where("id", $id)->delete();
        
        $response = [
            "errorId" => $success ? 0 : 1,
            "message" => $success ? "Data berhasil disimpan" : "Data gagal disimpan"
        ];

        return response()->json($response);
    }

    public function get_front(){
            $kegiatan =Kegiatan::get();      
        return view('kegiatan', compact('kegiatan'));
    }
}
