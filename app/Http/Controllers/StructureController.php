<?php

namespace App\Http\Controllers;

use Auth;
use App\Structure;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Input;
use DB;
use Config;
use Intervention\Image\ImageManagerStatic as Image;



class StructureController extends Controller
{
    public function index()
    {
        return view('structure.index');
    }

    public function add($id="")
    {
        $data['id'] = $id;
        return view('structure.add',$data);
    }
    
    public function get_datatables()
    {
        $structure = Structure::select(['structures.id','structures.parent_id', 'structures.name', 'structures.jabatan', 'structures.foto', 'structures.created_at']);

        return Datatables::of($structure)
            ->addColumn('foto', function ($structure) {
                
                if($structure->foto!=null || $structure->foto!=""){
                    
                    return '<a href="'.asset('assets/uploads/'.$structure->foto).'" data-lightbox="image-'.$structure->id.'" data-title="image-'.$structure->name.'"><img height="100" width="100" src="'.asset('assets/uploads/'.$structure->foto).'" class="img img-circle"></a>';
                }elseif($structure->foto != 'undefined'){
                    return "";
                }else{
                    return "";
                }
            })
            ->addColumn('action', function ($structure) {
                return '<a href="'.url('strukture/add/'.$structure->id).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a> <a href="javascript:;" onclick="delete_row('.$structure->id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>';
            })
            ->rawColumns(['action', 'foto'])
            ->removeColumn('updated_at')
            ->make(true);
    }

    public function get_edit($id=""){
        $data = [];

        if($id!=""){
            $structure = \App\Structure::find($id);
            
            if($structure->parent_id != null || $structure->parent_id != ""){
                $structure = DB::table('structures as a')
                    ->join('structures as b', function ($join) {
                        $join->on('a.parent_id', '=', 'b.id');
                    })
                    ->select('a.*','b.name as parent_name')
                    ->where('a.id', '=', $id)
                    ->first();
            }           

            

            $structure = json_decode(json_encode($structure),true);
        
            $data['edit'] = $structure;
        }
        
        return response()->json($data);
    }
          
    public function create(Request $request)
    {
        $data = request()->all();
        unset($data['X-CSRF-TOKEN']);
        unset($data['_token']);
        
        
        if(! empty($_FILES) && isset($_FILES['file']['name'])){
            $file = $_FILES['file'];
            $insert_id = date('d-m-YHis');
            $tempFile = $file['tmp_name'];
            
            $extension = $request->file('file')->extension();
            $uploads_dir = Config::get('app.upload_dir');            
            
            move_uploaded_file($tempFile, "$uploads_dir/$insert_id.".$extension);
            $data['foto'] = $insert_id.".".$extension;

            Image::make("$uploads_dir/$insert_id.".$extension)->resize(300, 200);
            unset($data['file']);
        }else{
            unset($data['file']);
        }
            
        if($data['id'] == '0' || empty($data['id'])){
            
            $success  = Structure::create($data);
        }else{
            $success  = Structure::where('id', $data['id'])
            ->update($data);
        }

        $response = [
            "errorId" => $success ? 0 : 1,
            "message" => $success ? "Data berhasil disimpan" : "Data gagal disimpan"
        ];

        return response()->json($response);
    }

    public function delete(Request $request)
    {
        $id = Input::get('id');
        $success = Structure::where("id", $id)->delete();
        
        $response = [
            "errorId" => $success ? 0 : 1,
            "message" => $success ? "Data berhasil disimpan" : "Data gagal disimpan"
        ];

        return response()->json($response);
    }

    public function get_parent()
    {
        //$search = $_GET['q'];
        //print_r($_GET);
        $response = \App\Structure::all();

        return response()->json($response);   
    }

    public function get_parent_tree()
    {
        $response = \App\Structure::where('parent_id', null)->get();
        
        $data = [];
        foreach ($response as $value) {
            $child = \App\Structure::where('parent_id', $value->id)->get()->count();            
            $children = ($child>0) ? true : false;

            array_push($data, [
                "id" => $value->id,
                "text" => $value->name,
                "icon" => "fa fa-user text-success",
                "children" => $children
            ]);
        }

        return response()->json($data);      
    }

    public function get_child_tree($id)
    {
        $response = \App\Structure::where('parent_id', $id)->get();

        $data = [];
        foreach ($response as $value) {
            $child = \App\Structure::where('parent_id', $value->id)->get()->count();            
            $children = ($child>0) ? true : false;

            array_push($data, [
                "id" => $value->id,
                "text" => $value->name,
                "icon" => "fa fa-user text-success",
                "children" => $children
            ]);
        }

        return response()->json($data); 
    }

}
