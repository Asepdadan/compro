<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StuctureController extends Controller
{
    //
    public function get_struktur($parent=null){
        $data = [];
        $result = \App\Structure::all();
        foreach($result as $row){
            array_push($data, [
                "id" => $row->id,
                "parent" => $row->parent_id,
                "description" => $row->name,
                "title" => $row->jabatan,
                "foto" => $row->foto,
                //"children" => $this->get_child($row->id),
                "templateName" => "contactTemplate",
                "itemTitleColor" => "red",
                "groupTitle"=> "Group 2"
            ]);
        }
        echo json_encode($data);
    }

  //   public function get_struktur1($parent=null){
		// $data = [];
		// $result = \App\Structure::where('parent_id',$parent)->get();
		
  //       foreach($result as $row){
		// 	//$data['children'] = $this->get_child($row->id); 
		// 	$rest = \App\Structure::where('parent_id',$row->id)->get();
  //           //dd($rest);

  //           if(! empty($rest)){
				
		// 		if($rest->count()>0){
		// 			// array_push($data, [
		// 			// 	"id" => $row->id,
		// 			// 	"name" => $row->name,
		// 			// 	"title" => $row->jabatan,
		// 			// 	"children" => $this->get_child($row->id)
		// 			// ]);

		// 			$data = [
		// 				"id" => $row->id,
  //                       "parent" => $row->parent_id,
		// 				"description" => $row->name,
		// 				"title" => $row->jabatan,
		// 				//"children" => $this->get_child($row->id),
  //                       "templateName" => "contactTemplate",
  //                       "itemTitleColor" => "red",
  //                       "groupTitle" =>  "Group 1",
		// 			];
		// 		}                
  //           }else{
  //               array_push($data, [
		// 			"id" => $row->id,
  //                   "parent" => $row->parent_id,
  //                   "description" => $row->name,
  //                   "title" => $row->jabatan,
  //                   "children" => $this->get_child($row->id),
  //                   "templateName" => "contactTemplate",
  //                   "itemTitleColor" => "red",
  //                   "groupTitle"=> "Group 2"
  //               ]);
  //           }       
  //       }
		
  //       echo json_encode($data);
  //   }

 //    private function get_child($parent){
 //        $result = \App\Structure::where('parent_id',$parent)->get();
        
 //        $data = [];
 //        foreach ($result as $value) {
            
 //            $rest = \App\Structure::where('parent_id',$value->id)->get();
 //            //dd($rest);

 //            if(! empty($rest)){
 //                if($rest->count()>0){
	// 				array_push($data, [
	// 					"id" => $row->id,
 //                        "parent" => $row->parent_id,
 //                        "description" => $row->name,
 //                        "title" => $row->jabatan,
 //                        "children" => $this->get_child($row->id),
 //                        "templateName" => "contactTemplate",
 //                        "itemTitleColor" => "red",
 //                        "groupTitle"=> "Group 2"
	// 					//"children" => $this->get_child($value->id),
	// 				]);
	// 			}else{
	// 				array_push($data, [
	// 					"id" => $row->id,
 //                        "parent" => $row->parent_id,
 //                        "description" => $row->name,
 //                        "title" => $row->jabatan,
 //                        "children" => $this->get_child($row->id),
 //                        "templateName" => "contactTemplate",
 //                        "itemTitleColor" => "red",
 //                        "groupTitle"=> "Group 2"
	// 				]);
	// 			}
                
 //            }else{
 //                array_push($data, [
	// 				"id" => $value->id,
 //                    "name" => $value->name,
 //                    "title" => $value->jabatan,
 //                ]);
 //            }
            
 //        }
        
 //       return $data;
	// }
	
	public function count_child($parent_id){
		return \App\Structure::where('parent_id',$parent_id)->get()->count();
	}

	public function foreach_child(){
		$rest = \App\Structure::where('parent_id',$parent_id)->get()->count();
		foreach ($result as $value) {
						
			array_push($data, [
				"name" => $value->name,
				"title" => $value->jabatan,
				"children" => $rest,
			]);
		}
	}

    // public function get_treeview_parent(){
	// 	$model = $this->M_Abc_Coa->get_parent($_GET['fiscal'], $_GET['company']);
		
	// 	$result = [];
		
	// 	foreach($model as $data){
	// 		$child = $this->M_Abc_Coa->count_child($data['ID']);
	// 		$children = ($child > 0)? true : false;
			
	// 		array_push($result, [
	// 			'id'=>$data['ID'],
	// 			'text'=>$data['ACTIVITY_NAME'],
	// 			'children'=>$children,
	// 			'icon'=>'fa fa-archive text-primary'
	// 		]);
	// 	}
		
	// 	header('Content-Type: application/json');
	// 	echo json_encode($result);
	// }
	
	// public function get_treeview_child(){
	// 	$model = $this->M_Abc_Coa->get_child($_GET['id']);
		
	// 	$result = [];
		
	// 	foreach($model as $data){
	// 		$child = $this->M_Abc_Coa->count_child($data['ID']);
	// 		$children = ($child > 0)? true : false;
			
	// 		array_push($result, [
	// 			'id'=>$data['ID'],
	// 			'text'=>$data['ACTIVITY_NAME'],
	// 			'children'=>$children,
	// 			'icon'=>'fa fa-archive text-primary'
	// 		]);
	// 	}
		
	// 	header('Content-Type: application/json');
	// 	echo json_encode($result);
	// }
}
