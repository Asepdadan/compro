<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{
    //
    protected $fillable = [
        'id','parent_id', 'name', 'jabatan', 'foto', 'created_at', 'updated_at'
    ];

}
