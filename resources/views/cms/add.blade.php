@extends('backend')
@section('title', 'Form Cms')

@push('css')
<link href="{{asset('assets/css/select2.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/bootstrap-summernote/summernote.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css">
@endpush
@section('content')
{{-- <div class="note note-success note-bordered">
    <p>
            GTreeTable is extension of Tweeter Bootstrap 3 framework, which allows to use tree structure inside HTML table. Full documentation is available <a href="https://github.com/gilek/bootstrap-gtreetable" target="_blank">here</a>
    </p>
</div> --}}

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-purple-plum">
            <i class="icon-lock font-purple-plum"></i>
            <span class="caption-subject bold uppercase">Form Cms</span>
            <span class="caption-helper">Content Management System</span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title="">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <form id="myForm"></form>        
    </div>
</div>
@endsection

@push('scripts')
<script id="entry-template" type="text/x-handlebars-template">
    <div class="row margin-top-10">
        <div class="col-md-6">
            <div class="form-group">
                <label class="text-uppercase">Code</label>
                <select class="form-control" id="code" name="code"></select>
            </div>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <div class="form-group">
                <label class="text-uppercase">short text</label>
                <input type="text" name="short_text" class="form-control" placeholder="short text" value="@{{edit.short_text}}">
            </div>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-12">
            <div class="form-group">
                <label class="text-uppercase">long text</label>
                <div id="summernote_1"></div>
            </div>
        </div>
    </div>

    <div class="row margin-top-20">
        <div class="col-md-3">
            <div class="form-group">
                <label class="text-uppercase">start date</label>
                <input autocomplete="off" type="text" class="form-control start-end-date" name="start_date" value="@{{edit.start_date}}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="text-uppercase">end date</label>
                <input autocomplete="off" type="text" class="form-control start-end-date" name="end_date" value="@{{edit.end_date}}">
            </div>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <div class="form-group">
                <label class="text-uppercase">File</label>
                <div>
                    <input type="file" id="imgupload1" style="display: none;" accept="image/*"> 
                    <img src="{{asset('assets/not_available.jpg')}}" width="200" height="172" border="1" class="img img-rounded" id="openImage1">
                </div>
            </div>
            <span class="help-text">Please click image to upload file</span>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="text-uppercase">active</label>
                <div class="radio-list">
                        
                    <label>
                        @{{#if_same edit.active '1'}} 
                        <input type="radio" name="active" id="optionsRadios1" value="1" checked>Aktif
                        @{{else}}
                        <input type="radio" name="active" id="optionsRadios1" value="1">Aktif
                        @{{/if_same}}
                    </label>
                    <label>
                        @{{#if_same edit.active '2'}} 
                        <input type="radio" name="active" id="optionsRadios2" value="2" checked>Tidak Aktif
                        @{{else}}
                        <input type="radio" name="active" id="optionsRadios2" value="2">Tidak Aktif
                        @{{/if_same}}
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-12">
            <div class="form-group">
                <button type="button" class="btn btn-success" id="save-button">Save</button>
                <button type="button" class="btn btn-default" id="cancel-button">Back</button>
            </div>
        </div>
    </div>

</script>

<script src="{{asset('assets/handlebars.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
<script src="{{asset('assets/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/jquery-validation/dist/additional-methods.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-summernote/summernote.min.js')}}"></script>
<script src="{{asset('assets/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

<script>
Handlebars.registerHelper('if_same', function(a, b , options) {
    if(a==b) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});
var id = "{{ isset($id) ? $id : 0 }}";
var source   = document.getElementById("entry-template").innerHTML;
var template = Handlebars.compile(source); 
$.ajax({
    url: "{{url('cms/get_edit')}}/"+id,
    type: 'GET',
    dataType: 'json',
})
.done(function(response) {
    $("#myForm").empty()
    
    $("#myForm").append(template(response))
    reinit(response)
    $("input[type='radio']").uniform()
})
.fail(function() {
    console.log("error");
})
.always(function() {
    console.log("complete");
});

var reinit = function(myData){
    $("#cancel-button").click(function(event) {
        window.history.back();
    });

    var form = $( "#myForm" );
    jQuery.validator.addMethod("validDate", function(value, element) {
        return this.optional(element) || moment(value,"DD-MM-YYYY").isValid();
    }, "Please enter a valid date in the format DD-MM-YYYY");
    form.validate({
        highlight: function(element) {

            $(element).closest('.form-group').addClass('has-error');

        },
        unhighlight: function(element) {

            $(element).closest('.form-group').removeClass('has-error');

        },
        success: function(element) {
            $(element).closest('.form-group').addClass('has-success');
        },
        errorElement: 'span',
        errorClass: 'help-block text-danger',
        errorPlacement: function(error, element) {
            if(element.parent('.form-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
            code: {
                required: true,
            },
            short_text: {
                required: true,
                minlength: 2,
                maxlength : 200,
            },
            start_date : {
                validDate: true
            },
            end_date : {
                validDate: true
            }
        },
    });

    var focused = "";
    var localName = "";

    $("#save-button").click(function(event) {
        if(form.valid() == false){
            console.log(form.validate())
            var dialog = bootbox.alert({
                title: '<i class="fa fa-warning"></i> Warning ',
                message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
                callback: function(result) {
                   $("select[name=short_text]").focus()
                    $('html, body').animate({
                        scrollTop: $(""+ localName +"[name='"+focused+"']").first().offset().top
                     }, 500);                          
                    // $(""+ localName +"[name='"+focused+"']").focus();
                } 
            });

            dialog.init(function(){
                setTimeout(function(){
                    var html = "";
                    var indexx = 0;
                    
                    form.validate().errorList.forEach(function(element, index){
                        html+= element.element.name.replace("_"," ").toUpperCase() +' : '+ element.message +'<br>'
                        
                        if(indexx == index){
                            focused = element.element.name;
                            localName = element.element.localName;
                        }

                        indexx++
                    })

                    
                    dialog.find('.bootbox-body').html(html);
                }, 1000);
            });

            return
        }
        
        var formData = new FormData();

        var formRawData = $('form').serializeArray();
        var json_data = {data:{}};
        
        formRawData.forEach(function(element) {
            if(element.value!=""){
                formData.append(element.name, element.value);
                json_data[element.name] = element.value;
            }
        });

        formData.append('id',id)
        formData.append("X-CSRF-TOKEN",$('meta[name="csrf-token"]').attr('content'))
        //if($("#summernote_1").summernote('code') != 'undefined'){
            formData.append('long_text',$("#summernote_1").code())
        //}        
        formData.append('file',$("#imgupload1")[0].files[0])

        var xhr = new XMLHttpRequest();

        xhr.open('POST', "{{url('cms/create')}}", true);

        var onerror = function(event) {
            toastr.error("Error");
        }

        xhr.onload = function () {
            if (xhr.status === 200) {
                console.log(xhr.responseText)
                response = JSON.parse(xhr.responseText);
                console.log(response)
                if(response.errorId == 0) {
                    bootbox.alert({ size: "small",message: response.message, callback: function(result) {
                        window.history.back()                            
                    } })
                    
                } else {
                    bootbox.alert({ size: "small",message: response.message, callback: function(result) {
                        window.history.back()                            
                    } })
                }
            }
        };

        xhr.send(formData);

        return false;
    });   

    $("#summernote_1").summernote({
        height: 300,
        placeholder : "long text enter"
    })

    $("#code").select2({
        allowClear: true,
        width:"100%",
        placeholder: 'Pilih Code',
        ajax: {
            url: "{{url('generics/get_parent')}}",
            dataType: 'json',
            delay: 250,
            quietMillis: 50,
            async:false,
            data: function (params) {
            return {
                q: params.term,
                page: params.page,
                type: params.type,
            };
        },
        processResults: function (data) {
            var rData = [];
            data.forEach(function(e) {
                
                rData.push({
                    'id': e['code'],
                    'text': e['code']+' - '+e['name'],
                });
            });
            
            return {
                results: rData
            };
        },
        cache: true
        }
    })

    if(myData.hasOwnProperty('edit')){
        $('#code').empty().append('<option value="'+ myData.edit.code +'">'+ myData.edit.generic_name +'</option>')
        .val(myData.edit.code).trigger('change')

        $("#summernote_1").code(myData.edit.long_text)
        if(myData.edit.file!=null){
            $("#openImage1").attr("src", "{{url('')}}/assets/uploads/"+myData.edit.file)    
        }        
    }    

    // jQuery.validator.setDefaults({
    //   debug: true,
    //   success: "valid"
    // });
    
    $('#openImage1').click(function(){ 
        $('#imgupload1').trigger('click'); 
    });

    $("#imgupload1").change(function() {
        readURL(this, "openImage1","imgupload1");
    });

    $(".start-end-date").datepicker({
        format : "dd-mm-yyyy",
        todayHighlight : true,
        autoclose : true,
    })
}

function readURL(input, image,image2) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#'+ image +'').attr('src', e.target.result);

            $('#'+ image2 +'').attr('value', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@endpush

