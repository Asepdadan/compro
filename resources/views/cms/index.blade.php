@extends('backend')
@section('title', 'List Cms')

@push('css')
<link href="{{asset('assets/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/datatables/fw/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css"> -->
<link href="{{asset('assets/css/select2.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/lightbox2-master/src/css/lightbox.css')}}" rel="stylesheet" type="text/css">
@endpush
@section('content')
{{-- <div class="note note-success note-bordered">
    <p>
            GTreeTable is extension of Tweeter Bootstrap 3 framework, which allows to use tree structure inside HTML table. Full documentation is available <a href="https://github.com/gilek/bootstrap-gtreetable" target="_blank">here</a>
    </p>
</div> --}}

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-purple-plum">
            <i class="icon-lock font-purple-plum"></i>
            <span class="caption-subject bold uppercase"> Master cms</span>
            <span class="caption-helper">Content Management System</span>
        </div>
        <div class="actions">
        <a href="{{url('cms/add')}}" class="btn btn-circle red-sunglo btn-sm" data-toggle="modal">
            <i class="fa fa-plus"></i> Add </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title="">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-hover table-condensed" id="table-cms">
            <thead>
                <tr>
                    <th>code</th>
                    <th>short text</th>
                    {{-- <th>long text</th> --}}
                    <th>file</th>
                    <th>create by</th>
                    <th>create at</th>
                    <th>create at</th>
                    <th>action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{asset('assets/datatables/datatables.min.js')}}"></script>
<script src="{{asset('assets/datatables/fw/js/dataTables.bootstrap.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
<script src="{{asset('assets/lightbox2-master/src/js/lightbox.js')}}"></script>
<script>
var id = "";

var tabel = $('#table-cms').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{url('cms/get_data_datatables')}}",
    columns: [
        {data: 'code'},
        {data: 'short_text'},
        //{data: 'long_text'},
        {data: 'file'},
        {data: 'create_by'},
        {data: 'created_at'},
        {data: 'active'},
        {data: 'action'},
    ],
    order: [[1, 'asc']]
});

function delete_row(rowid){
    var rowid = rowid;
    bootbox.confirm({ 
        size: "small",
        title: "Apakah anda yakin?",
        message: "data akan di hapus", 
        callback: function(result){ 
            if(result){
                $.ajax({
                    url: "{{url('cms/delete')}}",
                    type: 'POST',
                    dataType: 'json',
                    data : {
                        "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content'),
                        "id" : rowid
                    }
                })
                .done(function(response) {
                    tabel.ajax.reload()
                })
            }else{
                tabel.ajax.reload()
            }
            
        }
    }) 
}
lightbox.option({
    'resizeDuration': 200,
    'wrapAround': true
})
</script>
@endpush