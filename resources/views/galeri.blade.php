<link href="{{ URL::to('assets/slider/thumbnail-slider.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('assets/slider/thumbnail-slider2.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ URL::to('assets/slider/thumbnail-slider.js') }}" type="text/javascript"></script>
<div id="thumbnail-slider">
<h3 style="text-align: center;">GALERI</h3>
<div class="inner" id="galeri">
<ul>
   @foreach($galeri as $row)
   <li>
      <a class="thumb" href="{{ URL::to('assets/uploads/'.$row->file) }}"></a>
   </li>
   @endforeach
</ul>