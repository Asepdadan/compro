@extends('backend')
@section('title', 'Form Generics')

@push('css')
<link href="{{asset('assets/css/select2.min.css')}}" rel="stylesheet" type="text/css">
@endpush
@section('content')
{{-- <div class="note note-success note-bordered">
    <p>
            GTreeTable is extension of Tweeter Bootstrap 3 framework, which allows to use tree structure inside HTML table. Full documentation is available <a href="https://github.com/gilek/bootstrap-gtreetable" target="_blank">here</a>
    </p>
</div> --}}

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-purple-plum">
            <i class="icon-lock font-purple-plum"></i>
            <span class="caption-subject bold uppercase">Form Master Generics</span>
            <span class="caption-helper">Master kunci kode</span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title="">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <form id="myForm"></form>        
    </div>
</div>
@endsection

@push('scripts')
<script id="entry-template" type="text/x-handlebars-template">
<div class="container-fluid">
    <div class="row margin-top-10">
        <div class="col-md-6">
            <div class="form-group">
                <label class="text-uppercase">Code</label>
                <input type="text" name="code" class="form-control" placeholder="enter code" value="@{{edit.code}}">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="text-uppercase">parent code</label>
                <select class="form-control" id="parent_code" name="parent_code"></select>
            </div>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <div class="form-group">
                <label class="text-uppercase">name</label>
                <input type="text" name="name" class="form-control" placeholder="enter name" value="@{{edit.name}}">
            </div>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-12">
            <div class="form-group">
                <button type="button" class="btn btn-success" id="save-button">Simpan</button>
                <button type="button" class="btn btn-default" id="cancel-button">Kembali</button>
            </div>
        </div>
    </div>
</div>
</script>

<script src="{{asset('assets/handlebars.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
<script src="{{asset('assets/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/jquery-validation/dist/additional-methods.min.js')}}"></script>


<script>
var id = "{{ isset($id) ? $id : 0 }}";
var source   = document.getElementById("entry-template").innerHTML;
var template = Handlebars.compile(source); 
$.ajax({
    url: "{{url('generics/get_edit')}}/"+id,
    type: 'GET',
    dataType: 'json',
})
.done(function(response) {
    $("#myForm").empty()
    
    $("#myForm").append(template(response))
    reinit(response)
})
.fail(function() {
    console.log("error");
})
.always(function() {
    console.log("complete");
});

var reinit = function(myData){
    $("#cancel-button").click(function(event) {
        window.history.back();
    });

    var form = $( "#myForm" );
    form.validate({
        highlight: function(element) {

            $(element).closest('.form-group').addClass('has-error');

        },
        unhighlight: function(element) {

            $(element).closest('.form-group').removeClass('has-error');

        },
        success: function(element) {
            $(element).closest('.form-group').addClass('has-success');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.form-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
            code: {
                required: true,
                minlength: 2,
                maxlength : 10,
            },
            name: {
                required: true,
                minlength: 2,
                maxlength : 50,
             },
        },
    });

    $("#save-button").click(function(event) {
        if(form.valid() == false){
            return
        }
        
        var formData = new FormData();

        var formRawData = $('form').serializeArray();
        var json_data = {data:{}};
        
        formRawData.forEach(function(element) {
            if(element.value!=""){
                formData.append(element.name, element.value);
                json_data[element.name] = element.value;
            }
        });

        formData.append('id',id)
        formData.append("X-CSRF-TOKEN",$('meta[name="csrf-token"]').attr('content'))

        var xhr = new XMLHttpRequest();

        xhr.open('POST', "{{url('generics/create')}}", true);

        var onerror = function(event) {
            toastr.error("Error");
        }

        xhr.onload = function () {
            if (xhr.status === 200) {
                response = JSON.parse(xhr.responseText);

                if(response.errorId == 0) {
                    bootbox.alert({ size: "small",message: response.message, callback: function(result) {
                        window.history.back()                            
                    } })
                    
                } else {
                    bootbox.alert({ size: "small",message: response.message, callback: function(result) {
                        window.history.back()                            
                    } })
                }
            }
        };

        xhr.send(formData);

        return false;
    });   

    $("#parent_code").select2({
        allowClear: true,
        width:"100%",
        placeholder: 'Pilih Parent',
        ajax: {
            url: "{{url('generics/get_parent')}}",
            dataType: 'json',
            delay: 250,
            quietMillis: 50,
            async:false,
            data: function (params) {
            return {
                q: params.term,
                page: params.page,
                type: params.type,
            };
        },
        processResults: function (data) {
            var rData = [];
            data.forEach(function(e) {
                
                rData.push({
                    'id': e['code'],
                    'text': e['code']+' - '+e['name'],
                });
            });
            
            return {
                results: rData
            };
        },
        cache: true
        }
    })

    if(myData.hasOwnProperty('edit')){
        $('#parent_code').empty().append('<option value="'+ myData.edit.parent_code +'">'+ myData.edit.parent_name +'</option>')
        .val(myData.edit.parent_code).trigger('change')
    }
    

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    
}

</script>
@endpush