@extends('backend')
@section('title', 'List Generics')

@push('css')
<link href="{{asset('assets/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/datatables/fw/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/select2.min.css')}}" rel="stylesheet" type="text/css">
@endpush
@section('content')
{{-- <div class="note note-success note-bordered">
    <p>
            GTreeTable is extension of Tweeter Bootstrap 3 framework, which allows to use tree structure inside HTML table. Full documentation is available <a href="https://github.com/gilek/bootstrap-gtreetable" target="_blank">here</a>
    </p>
</div> --}}

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-purple-plum">
            <i class="icon-lock font-purple-plum"></i>
            <span class="caption-subject bold uppercase"> Master Generics</span>
            <span class="caption-helper">Master kunci kode</span>
        </div>
        <div class="actions">
        <a href="{{url('generics/add')}}" class="btn btn-circle red-sunglo btn-sm" data-toggle="modal">
            <i class="fa fa-plus"></i> Add </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title="">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-hover table-responsive" id="table-generics">
            <thead>
                <tr>
                    <th>code</th>
                    <th>parent code</th>
                    <th>name</th>
                    <th>active</th>
                    <th>create at</th>
                    <th>action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>


@endsection


@push('scripts')
<script id="entry-template" type="text/x-handlebars-template">
<div class="container-fluid">
    <div class="row margin-top-10">
        <div class="col-md-12">
            <div class="form-group">
                <label class="text-uppercase">Code</label>
                <input type="text" name="code" class="form-control" placeholder="enter code" value="@{{edit.code}}">
            </div>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-12">
            <div class="form-group">
                <label class="text-uppercase">parent code</label>
                <select class="form-control" id="parent_code" name="parent_code"></select>
            </div>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-12">
            <div class="form-group">
                <label class="text-uppercase">name</label>
                <input type="text" name="name" class="form-control" placeholder="enter name" value="@{{edit.name}}">
            </div>
        </div>
    </div>
</div>
</script>
<script src="{{asset('assets/datatables/datatables.min.js')}}"></script>
<script src="{{asset('assets/datatables/fw/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/handlebars.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
<script>
var id = "";
var source   = document.getElementById("entry-template").innerHTML;
var template = Handlebars.compile(source);  
var tabel = $('#table-generics').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{url('generics/get_data_datatables')}}",
    columns: [
        {data: 'code'},
        {data: 'parent_code'},
        {data: 'name'},
        {data: 'active'},
        {data: 'created_at'},
        {data: 'action'},
    ],
    order: [[1, 'asc']]
});

function delete_row(rowid){
    var rowid = rowid;
    bootbox.confirm({ 
        size: "small",
        title: "Apakah anda yakin?",
        message: "data akan di hapus", 
        callback: function(result){ 
            if(result){
                $.ajax({
                    url: "{{url('generics/delete')}}",
                    type: 'POST',
                    dataType: 'json',
                    data : {
                        "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content'),
                        "id" : rowid
                    }
                })
                .done(function(response) {
                    tabel.ajax.reload()
                })
            }else{
                tabel.ajax.reload()
            }
            
        }
    }) 
}
</script>
@endpush