<!DOCTYPE html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
   <meta charset="utf-8">
   <title>HUMAS - KOTA BANDUNG</title>
   <meta name="author" content="iltaen">
   <meta name="keywords" content="creative, modern, minimal, elegant, clear, parallax, clean, dark, circle, portfolio, personal, orange, photo">
   <meta name="description" content="Creative portfolio HTML template">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- CSS -->
   <meta http-equiv="X-UA-Compatible" content="edge"/>
   <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
   <link rel="stylesheet" type="text/css" href="{{ URL::to('assets/c-fonts/font-awesome/css/font-awesome.css') }}" />
   <link rel="stylesheet" type="text/css" href="{{ URL::to('assets/styles/basic.css') }}" />
   <link rel="stylesheet" type="text/css" media="only screen and (max-width: 1024px)" href="{{ URL::to('assets/styles/mobile.css') }}" />
   <link href="{{ URL::to('assets/slider/thumbnail-slider.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{ URL::to('assets/slider/thumbnail-slider2.css') }}" rel="stylesheet" type="text/css" />
   <link href="{{asset('assets/OrgChart-master/OrgChart-master/src/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
   <link href="{{asset('assets/OrgChart-master/OrgChart-master/src/css/jquery.orgchart.css')}}" rel="stylesheet" type="text/css">
   <link href="{{asset('assets/OrgChart-master/OrgChart-master/src/css/style.css')}}" rel="stylesheet" type="text/css">
   <link href="{{asset('assets/orgprimitives/packages/jquery-ui/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="{{asset('assets/orgprimitives/min/primitives.latest.css')}}">
   <script src="{{ URL::to('js/jquery.min.js') }}"></script>
   <script src="{{ URL::to('js/masonry.pkgd.min.js') }}"></script>
   <script src="{{ URL::to('js/script.js') }}" type="text/javascript"></script>
   <script src="{{ URL::to('assets/slider/thumbnail-slider.js') }}" type="text/javascript"></script>
   <script src="{{asset('assets/handlebars.js')}}"></script>
   <script src="{{asset('assets/OrgChart-master/OrgChart-master/src/js/jquery.mockjax.min.js')}}"></script>
   <script src="{{asset('assets/OrgChart-master/OrgChart-master/src/js/jquery.orgchart.js')}}"></script>
   <script src="{{asset('assets/orgprimitives/packages/jquery/jquery-3.3.1.min.js')}}"></script>
   <script src="{{asset('assets/orgprimitives/packages/jquery-ui/jquery-ui.min.js')}}"></script>
   <script src="{{asset('assets/orgprimitives/min/primitives.min.js')}}"></script>
   <style>
      #overlay{
      position: fixed;
      z-index: 1000;
      left: 0;
      top: 100px;
      width: 25px;
      height: 20px;
      border-radius: 0 20px 20px 0;
      padding: 5px;
      background: rgba(242, 101, 34, 0.8);
      overflow: hidden;
      -webkit-transition: all 0.5s ease;
      -moz-transition: all 0.5s ease;
      -ms-transition: all 0.5s ease;
      -o-transition: all 0.5s ease;
      transition: all 0.5s ease;
      }
      #overlay:hover{
      width: 200px;
      }
      #overlay a{
      text-shadow: none;
      margin-left: 5px;
      font-size: 14px;
      width: 100%;
      float: left;
      color: #fff;
      }
   </style>
   <style>
      .card {
      box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
      transition: 0.3s;
      width: 100%;
      border-radius: 5px;
      }
      .card:hover {
      box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
      }
      img {
      border-radius: 5px 5px 0 0;
      }
      .container-card {
      padding: 2px 16px;
      }
      .grid-container {
      display: grid;
      padding: 20px;
      }
      .grid-item {
      padding: 20px;
      text-align: center;
      height: auto;
      }
   </style>
</head>
<body>
   <div id="background"></div>
   <div id="texture"></div>
   <div class="fullcontent">
      <div class="menuwrapper">
         <div class="mainmenu">
            <div class="menucenter">
               @if ($message = Session::get('success'))
               <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{{ $message }}</strong>
               </div>
               @endif
               <div class="logo-container">
                  <img class="logo" alt="logotype" src="{{ URL::to('assets/img/logo.png') }}">
                  <img class="photo" alt="photo" src="{{ URL::to('assets/img/photo.png') }}">
               </div>
            </div>
            <div class="menu-items">
               <a href="{{ URL::to('tentang') }}" class="menuitem">
                  <i class="icon-info"></i>
                  <div class="menu-title">Tentang</div>
               </a>
               <a href="{{ URL::to('front-kegiatan') }}" class="menuitem">
                  <i class="icon-calendar"></i>
                  <div class="menu-title">Kegiatan</div>
               </a>
               <a href="{{ URL::to('galeri') }}" class="menuitem bigpage">
                  <i class="icon-picture"></i>
                  <div class="menu-title">Galeri</div>
               </a>
               <a href="{{ URL::to('struktur') }}" class="menuitem">
                  <i class="icon-user"></i>
                  <div class="menu-title">Struktur</div>
               </a>
               <a href="{{ URL::to('kontak') }}" class="menuitem">
                  <i class="icon-envelope"></i>
                  <div class="menu-title">Kontak</div>
               </a>
            </div>
            <div id="linecontainer" class="select-about">
               <div class="linewrapper">
                  <div class="overwrapper">
                     <div class="colorlines large">
                        <div class="colorlines small"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="page">
         <div class="loader">
            <div class="label glcolor">
            </div>
         </div>
         <div class="wrapper">
            <div class="table">
               <div class="table-cell">
                  <div class="container">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</body>
</html>