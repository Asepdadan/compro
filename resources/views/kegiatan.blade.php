<h2 style="text-align: center;">DAFTAR KEGIATAN</h2>
<div class="grid-container">
	@foreach($kegiatan as $row)
	<div class="grid-item">
		<div class="card">
		  <div class="container-card">
		    <h4>{{ $row->judul }}</h4><hr>
		    <p>Waktu : {{ $row->waktu }}</p>
		    <p>Tempat : {{ $row->tempat }}</p>
		    <p>Keterangan : {{ $row->keterangan }}</p>
		  </div>
		</div>
	</div>
	@endforeach
</div>