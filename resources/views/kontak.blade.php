<link href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<h1 class="text-center">Kontak Kami</h1>
<div class="section contacts">
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
    </div>
    @endif
   <form class="email" action="{{ route('send.mail') }}" method="post">
      {{ csrf_field() }}
      {{ method_field('post') }}
      <div >
         <div class="input-icon">
            <i class="icon-envelope"></i>
            <input type="text" name="s-email" placeholder="YOUR E-MAIL" class="mail">
         </div>
         <div class="input-icon">
            <i class="icon-user"></i>
            <input type="text" name="s-name" placeholder="YOUR NAME" class="name">
         </div>
         <input type="text" name="s-subject" placeholder="SUBJECT" class="w100 mtop10 fsize90">
         <textarea name="s-message" class="message" placeholder="YOUR MESSAGE"></textarea>
      </div>
      <div class="mtop10">
         <div class="fright">
            <button name="submit" type="submit">KIRIM</button>
         </div>
      </div>
   </form>
</div>
<br><br>
<br>
<div class="row">
   <div class="col-md-6">
      <p><i class="icon-globe"></i> humas.bandung.go.id</p>
      <p><i class="icon-home"></i> Jl. Wastukancana No. 2 Bandung, Jawa Barat</p>
      <p><i class="icon-phone"></i> 022 - 4210858 </p>
   </div>
   <div class="col-md-6">
      <p><i class="icon-envelope"></i> humbandung@gmail.com</p>
      <p><i class="icon-twitter"></i> @HumasBdg</p>
      <p><i class="icon-facebook"></i> Humas Kota Bandung</p>
   </div>
</div>