@extends('backend')
@section('title', 'Form Struktur')

@push('css')
<link href="{{asset('assets/css/select2.min.css')}}" rel="stylesheet" type="text/css">

@endpush
@section('content')
{{-- <div class="note note-success note-bordered">
    <p>
            GTreeTable is extension of Tweeter Bootstrap 3 framework, which allows to use tree structure inside HTML table. Full documentation is available <a href="https://github.com/gilek/bootstrap-gtreetable" target="_blank">here</a>
    </p>
</div> --}}

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-purple-plum">
            <i class="icon-lock font-purple-plum"></i>
            <span class="caption-subject bold uppercase">Form Stuktur Organigram</span>
            <span class="caption-helper">Struktur Organigram</span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title="">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <form id="myForm"></form>        
    </div>
</div>
@endsection

@push('scripts')
<script id="entry-template" type="text/x-handlebars-template">
    <div class="row margin-top-10">
        <div class="col-md-6">
            <div class="form-group">
                <label class="text-uppercase">Parent</label>
                <select class="form-control" id="parent" name="parent_id"></select>
            </div>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <div class="form-group">
                <label class="text-uppercase">Nama</label>
                <input type="text" name="name" class="form-control" placeholder="nama" value="@{{edit.name}}">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="text-uppercase">Jabatan</label>
                <input type="text" name="jabatan" class="form-control" placeholder="jabatan" value="@{{edit.jabatan}}">
            </div>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-md-6">
            <div class="form-group">
                <label class="text-uppercase">File</label>
                <div>
                    <input type="file" id="imgupload1" style="display: none;" accept="image/*"> 
                    <img src="{{asset('assets/not_available.jpg')}}" width="200" height="172" border="1" class="img img-rounded" id="openImage1">
                </div>
            </div>
            <span class="help-text">Please click image to upload file</span>
        </div>
        {{-- <div class="col-md-2">
            <div class="form-group">
                <label class="text-uppercase">active</label>
                <div class="radio-list">
                        
                    <label>
                        @{{#if_same edit.active '1'}} 
                        <input type="radio" name="active" id="optionsRadios1" value="1" checked>Aktif
                        @{{else}}
                        <input type="radio" name="active" id="optionsRadios1" value="1">Aktif
                        @{{/if_same}}
                    </label>
                    <label>
                        @{{#if_same edit.active '2'}} 
                        <input type="radio" name="active" id="optionsRadios2" value="2" checked>Tidak Aktif
                        @{{else}}
                        <input type="radio" name="active" id="optionsRadios2" value="2">Tidak Aktif
                        @{{/if_same}}
                    </label>
                </div>
            </div>
        </div> --}}
    </div>

    <div class="row margin-top-10">
        <div class="col-md-12">
            <div class="form-group">
                <button type="button" class="btn btn-success" id="save-button">Save</button>
                <button type="button" class="btn btn-default" id="cancel-button">Back</button>
            </div>
        </div>
    </div>

</script>
@push('scripts')
<script src="{{asset('assets/handlebars.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>


<script>
Handlebars.registerHelper('if_same', function(a, b , options) {
    if(a==b) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});
var id = "{{ isset($id) ? $id : 0 }}";
var source   = document.getElementById("entry-template").innerHTML;
var template = Handlebars.compile(source); 
$.ajax({
    url: "{{url('strukture/get_edit')}}/"+id,
    type: 'GET',
    dataType: 'json',
})
.done(function(response) {
    $("#myForm").empty()
    
    $("#myForm").append(template(response))
    reinit(response)
    $("input[type='radio']").uniform()
})
.fail(function() {
    console.log("error");
})
.always(function() {
    console.log("complete");
});

var reinit = function(myData){
    $("#cancel-button").click(function(event) {
        window.history.back();
    });

    var focused = "";
    var localName = "";

    $("#save-button").click(function(event) {        
        var formData = new FormData();

        var formRawData = $('form').serializeArray();
        var json_data = {data:{}};
        
        formRawData.forEach(function(element) {
            if(element.value!=""){
                formData.append(element.name, element.value);
                json_data[element.name] = element.value;
            }
        });

        formData.append('id',id)
        formData.append("X-CSRF-TOKEN",$('meta[name="csrf-token"]').attr('content'))
        formData.append('file',$("#imgupload1")[0].files[0])

        var xhr = new XMLHttpRequest();

        xhr.open('POST', "{{url('strukture/create')}}", true);

        var onerror = function(event) {
            toastr.error("Error");
        }

        xhr.onload = function () {
            if (xhr.status === 200) {
                console.log(xhr.responseText)
                response = JSON.parse(xhr.responseText);
                console.log(response)
                if(response.errorId == 0) {
                    bootbox.alert({ size: "small",message: response.message, callback: function(result) {
                        window.history.back()                            
                    } })
                    
                } else {
                    bootbox.alert({ size: "small",message: response.message, callback: function(result) {
                        window.history.back()                            
                    } })
                }
            }
        };

        xhr.send(formData);

        return false;
    });   


    $("#parent").select2({
        allowClear: true,
        width:"100%",
        placeholder: 'Pilih Parent',
        ajax: {
            url: "{{url('strukture/get_parent')}}",
            dataType: 'json',
            delay: 250,
            quietMillis: 50,
            async:false,
            data: function (params) {
            return {
                q: params.term,
                page: params.page,
                type: params.type,
            };
        },
        processResults: function (data) {
            var rData = [];
            data.forEach(function(e) {
                
                rData.push({
                    'id': e['id'],
                    'text': e['id']+' - '+e['name'],
                });
            });
            
            return {
                results: rData
            };
        },
        cache: true
        }
    })

    if(myData.hasOwnProperty('edit')){
        $('#parent').empty().append('<option value="'+ myData.edit.parent_id +'">'+ myData.edit.parent_name +'</option>')
        .val(myData.edit.parent_id).trigger('change')

        if(myData.edit.foto!=null){
            $("#openImage1").attr("src", "{{url('')}}/assets/uploads/"+myData.edit.foto)    
        }        
    }    
    
    $('#openImage1').click(function(){ 
        $('#imgupload1').trigger('click'); 
    });

    $("#imgupload1").change(function() {
        readURL(this, "openImage1","imgupload1");
    });

}
function readURL(input, image,image2) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#'+ image +'').attr('src', e.target.result);

            $('#'+ image2 +'').attr('value', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

</script>
@endpush
