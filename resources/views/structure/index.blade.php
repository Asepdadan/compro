@extends('backend')
@section('title', 'List Organigram')

@push('css')
<link href="{{asset('assets/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/datatables/fw/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css"> -->
<link href="{{asset('assets/css/select2.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/lightbox2-master/src/css/lightbox.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/jstree/src/themes/default/style.css')}}" rel="stylesheet" type="text/css">

<link href="{{asset('assets/orgprimitives/packages/jquery-ui/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{asset('assets/orgprimitives/min/primitives.latest.css')}}">
<link rel="stylesheet" href="{{asset('assets/orgprimitives/packages/bootstrap/css/bootstrap-responsive.min.css')}}">
@endpush
@section('content')
{{-- <div class="note note-success note-bordered">
    <p>
            GTreeTable is extension of Tweeter Bootstrap 3 framework, which allows to use tree structure inside HTML table. Full documentation is available <a href="https://github.com/gilek/bootstrap-gtreetable" target="_blank">here</a>
    </p>
</div> --}}
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-purple-plum">
            <i class="icon-lock font-purple-plum"></i>
            <span class="caption-subject bold uppercase">Tree View Master Struktur</span>
            <span class="caption-helper">Organigram Humas Kota Bandung</span>
        </div>
        
    </div>
    <div class="portlet-body">
        <div class="container">
            <div class="row margin-top-20 col-md-6">
                <div id="tree"></div>
            </div>
            <div class="row margin-top-20 col-md-6">
                <div id="basicdiagram" style="width: 640px; height: 480px; border-style: dotted; border-width: 1px;" ></div>
            </div>
        </div>
        
    </div>
</div>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-purple-plum">
            <i class="icon-lock font-purple-plum"></i>
            <span class="caption-subject bold uppercase"> Master Struktur</span>
            <span class="caption-helper">Organigram Humas Kota Bandung</span>
        </div>
        <div class="actions">
        <a href="{{url('strukture/add')}}" class="btn btn-circle red-sunglo btn-sm" data-toggle="modal">
            <i class="fa fa-plus"></i> Add </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title="">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-hover table-condensed" id="table-struktur">
            <thead>
                <tr>
                    <th>id</th>
                    <th>parent id</th>
                    <th>nama</th>
                    <th>jabatan</th>
                    <th>foto</th>
                    <th>create at</th>
                    <th>action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>


@endsection

@push('scripts')
<script src="{{asset('assets/datatables/datatables.min.js')}}"></script>
<script src="{{asset('assets/datatables/fw/js/dataTables.bootstrap.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
<script src="{{asset('assets/lightbox2-master/src/js/lightbox.js')}}"></script>
<script src="{{asset('assets/jstree/src/jstree.js')}}"></script>

<script src="{{asset('assets/orgprimitives/packages/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/orgprimitives/min/primitives.min.js')}}"></script>

<script>
var id = "";
var tabel = $('#table-struktur').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{url('strukture/get_data_datatables')}}",
    columns: [
        {data: 'id'},
        {data: 'parent_id'},
        {data: 'name'},
        {data: 'jabatan'},
        {data: 'foto'},
        {data: 'created_at'},
        {data: 'action'},
    ],
    order: [[1, 'asc']]
});

function delete_row(rowid){
    var rowid = rowid;
    bootbox.confirm({ 
        size: "small",
        title: "Apakah anda yakin?",
        message: "data akan di hapus", 
        callback: function(result){ 
            if(result){
                $.ajax({
                    url: "{{url('strukture/delete')}}",
                    type: 'POST',
                    dataType: 'json',
                    data : {
                        "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content'),
                        "id" : rowid
                    }
                })
                .done(function(response) {
                    tabel.ajax.reload()
                })
            }else{
                tabel.ajax.reload()
            }
            
        }
    }) 
}
lightbox.option({
    'resizeDuration': 200,
    'wrapAround': true
})

// function tree(argument) {
    $('#tree').jstree({
        'core' : {
          'data' : {
            'url' : function (node) {
                
              return node.id === '#' ? '{{url('strukture/get_parent_tree')}}' : '{{url('strukture/get_child_tree')}}/'+node.id;
            },
            'data' : function (node) {
              return { 'id' : node.id };
            }
          }
        }
    });
//}

$(document).ready(function() {
    var datas = {};
    var items = [];
    var items1 = [];
    var ajax_load = function(callback){
        $.ajax({
          url : "{{url('get_struktur')}}",
          type : 'get',
          dataType : 'json',
          async : false,
          success : function(response){
            callback(response)
          }
        })
    }

    ajax_load(function(response) {
        response.forEach( function(element, index) {
            items.push({
                  id: element.id, 
                  parent: element.parent, 
                  title: element.title, 
                  description: element.description,
                  image: "{{url('')}}/assets/uploads/"+element.foto
            })
        })
    });

    var options = new primitives.orgdiagram.Config();
    options.onMouseDblClick = function (e, data) {
        alert("User clicked on item '" + data.context.title + "'.");
    }

    jQuery("#basicdiagram").orgDiagram({
        items: items,
        cursorItem: 0,
        hasSelectorCheckbox: primitives.common.Enabled.false,
        options : options,
        graphicsType: primitives.common.GraphicsType.SVG,
        pageFitMode: primitives.common.PageFitMode.FitToPage,
        verticalAlignment: primitives.common.VerticalAlignmentType.Middle,
        connectorType: primitives.common.ConnectorType.Angular,
        minimalVisibility: primitives.common.Visibility.Dot,
        selectionPathMode: primitives.common.SelectionPathMode.FullStack,
        leavesPlacementType: primitives.common.ChildrenPlacementType.Horizontal,
        hasButtons: primitives.common.Enabled.Auto,
        hasSelectorCheckbox: primitives.common.Enabled.False,
        buttons: [
            new primitives.orgdiagram.ButtonConfig("Edit", "ui-icon-gear", "Edit"),
        ],
        onButtonClick: onButtonClick,
    });
});
function onButtonClick(e, data) {
    switch(data.name){
        case 'Edit':
            window.location.href = "{{url('/strukture/add')}}/"+data.context.id;
        break;
        case 'Edit':
            window.location.href = "{{url('/strukture/add')}}/"+data.context.id;
        break;
        
    }
}
</script>


@endpush