<style type="text/css">
   #chart-container {
   font-family: Arial;
   height: 720px;
   border: 2px dashed #fff;
   border-radius: 5px;
   widows: 200px;
   width: 100%;
   overflow: auto;
   text-align: center;
   }
   #github-link {
   position: fixed;
   top: 0px;
   right: 10px;
   font-size: 3em;
   }
   .orgchart .second-menu-icon {
   transition: opacity .5s;
   opacity: 0;
   right: -5px;
   top: -5px;
   z-index: 2;
   color: rgba(68, 157, 68, 0.5);
   font-size: 18px;
   position: absolute;
   }
   .orgchart .second-menu-icon:hover { color: #fff; }
   .orgchart .node:hover .second-menu-icon { opacity: 1; }
   .orgchart .node .second-menu {
   display: none;
   position: absolute;
   top: 0;
   right: -70px;
   border-radius: 35px;
   box-shadow: 0 0 10px 1px #999;
   background-color: #fff;
   z-index: 1;
   }
   .orgchart .node .second-menu .avatar {
   width: 60px;
   height: 60px;
   border-radius: 30px;
   float: left;
   margin: 5px;
   }
</style>
<script type="text/javascript">
   (function($) {
    $(function() {
   
      var datas = {};
      var ajax_load = function(callback){
        $.ajax({
          url : "{{url('get_struktur')}}",
          type : 'get',
          dataType : 'json',
          async : false,
          success : function(response){
            callback(response)
          }
        })
      }
   
      ajax_load(function(response) {
        //datas = response;
        var items = [];
        var items1 = [];
        response.forEach( function(element, index) {
          items.push({
              id: element.id, 
              parent: element.parent, 
              title: element.title, 
              description: element.description,
              image: "{{url('')}}/assets/uploads/"+element.foto
          })

        });

        var options = new primitives.orgdiagram.Config();
        options.onCursorChanged = function (e, data) {
          alert("User clicked on item '" + data.context.title + "'.");
        }

        jQuery("#basicdiagram").orgDiagram({
          items: items,
          cursorItem: 0,
          hasSelectorCheckbox: primitives.common.Enabled.false,
          options : options,
        });
        
      });
   
      // var oc = $('#chart-container').orgchart({
      //   'data' : datas,
      //   'depth': 2,
      //   'nodeContent': 'title',
      //   'nodeID': 'id',
      //   'nodeTemplate': nodeTemplate,
      //   'createNode': function($node, data) {
      //     var secondMenuIcon = $('<i>', {
      //       'class': 'fa fa-info-circle second-menu-icon',
      //       click: function() {
      //         $(this).siblings('.second-menu').toggle();
      //       }
      //     });
      //     var secondMenu = '<div class="second-menu"><img class="avatar" src=""></div>';
      //     $node.append(secondMenuIcon).append(secondMenu);
      //   }
      // });
   
      // var nodeTemplate = function(data) {
      //   return `
      //     <span class="office">test</span>
      //     <div class="title">${data.name}</div>
      //     <div class="content">
      //       <img class="avatar" src=""> ${data.title}
      //     </div>
      //   `;
      // };
   
   
    });
   })(jQuery);
</script>
  
</script>
<div id="basicdiagram" style="width: 640px; height: 480px; border-style: dotted; border-width: 1px;" />
