<link href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<style type="text/css">
   #kontent{
   color: white !important;
   }
</style>
<h1 class=" text-center">HUMAS <span class="glcolor">KOTA BANDUNG</span></h1>
<div id="kontent"></div>
<script>
   $.ajax({
       url : "{{ url('cms/get_front') }}/{{config('kode.tentang')}}",
       type : "GET",
       dataType : "json",
       success : function(response){
           $('#kontent').html(response.long_text)
       }
   })
</script>