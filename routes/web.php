<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('tentang', function () {
    return view('tentang');
});
Route::get('kontak', function () {
    return view('kontak');
});
Route::get('galeri', function () {
    $response['galeri'] = \App\Cms::where('code', config('kode.galeri'))->where('active', 1)->where('file','!=', null)->get();
    return view('galeri', $response);
});
Route::get('front-kegiatan','KegiatanController@get_front');

Route::get('get_struktur','StuctureController@get_struktur');

Route::get('struktur', function () {
    
    return view('struktur');
});

Route::get('home', function(){
    return get('tentang');
});
Route::post('mail', 'MailController@sendmail')->name('send.mail');

Auth::routes();

Route::group(['middleware' => ['auth']], function (){
    Route::get('/home', 'HomeController@index')->name('home');

    $url = "generics";
    Route::group(['prefix' => 'generics'], function () use($url){        	
        if($url == 'generics')
        {
            Route::get('/', 'GenericController@index');
            Route::get('add/{id?}', 'GenericController@add');
            Route::get('get_data_datatables', 'GenericController@get_datatables');
            Route::get('get_edit/{id?}', 'GenericController@get_edit');
            Route::get('get_parent', 'GenericController@get_parent');
            Route::post('create', 'GenericController@create');
            Route::post('delete', 'GenericController@delete');
        }
    });

    $cms = "cms";
    Route::group(['prefix' => 'cms'], function () use($cms){        	
        if($cms == 'cms')
        {
            Route::get('/', 'CmsController@index');
            Route::get('add/{id?}', 'CmsController@add');
            Route::get('get_data_datatables', 'CmsController@get_datatables');
            Route::get('get_edit/{id?}', 'CmsController@get_edit');
            Route::get('get_parent', 'CmsController@get_parent');
            Route::post('create', 'CmsController@create');
            Route::post('delete', 'CmsController@delete');
            Route::get('get_front/{id?}', 'CmsController@get_front');
        }
    });

    $kegiatan = "kegiatan";
    Route::group(['prefix' => 'kegiatan'], function () use($kegiatan){            
        if($kegiatan == 'kegiatan')
        {
            Route::get('/', 'KegiatanController@index');
            Route::get('add/{id?}', 'KegiatanController@add');
            Route::get('get_data_datatables', 'KegiatanController@get_datatables');
            Route::get('get_edit/{id?}', 'KegiatanController@get_edit');
            Route::get('get_parent', 'KegiatanController@get_parent');
            Route::post('create', 'KegiatanController@create');
            Route::post('delete', 'KegiatanController@delete');
            Route::get('get_front/{id?}', 'KegiatanController@get_front');
        }
    });

    $strukture = "strukture";
    Route::group(['prefix' => 'strukture'], function () use($strukture){            
        if($strukture == 'strukture')
        {
            Route::get('/', 'StructureController@index');
            Route::get('add/{id?}', 'StructureController@add');
            Route::get('get_data_datatables', 'StructureController@get_datatables');
            Route::get('get_edit/{id?}', 'StructureController@get_edit');
            Route::get('get_parent', 'StructureController@get_parent');
            Route::post('create', 'StructureController@create');
            Route::post('delete', 'StructureController@delete');
            Route::get('get_parent_tree', 'StructureController@get_parent_tree');
            Route::get('get_child_tree/{id?}', 'StructureController@get_child_tree');
        }
    });
});




